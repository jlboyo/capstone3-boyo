import React,{useContext} from 'react'
import Navbar from 'react-bootstrap/Navbar'
import Nav from 'react-bootstrap/Nav'
import {NavLink, Link, Redirect} from 'react-router-dom'
import UserContext from '../userContext'
import '../App.css'

export default function NavBar(){

const {user,unsetUser,setUser} = useContext(UserContext)

function logout(){
	unsetUser()
	setUser({
		email:null,
		isAdmin:null
	})
	window.location.replace('/')
}

	return (

		<Navbar expand="lg" className="mainbar">
				<Navbar.Brand as={Link} to="/" >Viisumus</Navbar.Brand>
			<Navbar.Toggle aria-controls="basic-navbar-nav" />
			<Navbar.Collapse id="basic-navbar-nav">
				<Nav className='ml-auto'>	
					{
						user.email
						? 	
						<>
							<Nav.Link as={NavLink} to="/categories" className='navtxt'>Categories</Nav.Link>
							<Nav.Link as={NavLink} to="/income">Income</Nav.Link>
							<Nav.Link as={NavLink} to="/expenses">Expenses</Nav.Link>
							{/* <Nav.Link as={NavLink} to="/monthly">Monthly</Nav.Link>
							<Nav.Link as={NavLink} to="/trend">Trend</Nav.Link> */}
							<Nav.Link onClick={logout}>Logout</Nav.Link>
						</>
						: 
						<>	
							<Nav.Link as={NavLink} to="/">Log In</Nav.Link>
							<Nav.Link as={NavLink} to="/register">Create New Account</Nav.Link>
							
						</>
					}
				</Nav>
			</Navbar.Collapse>
		</Navbar>
	)
}