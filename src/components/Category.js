import React,{useState,useEffect,useContext} from 'react'

import {Card,Button} from 'react-bootstrap'

export default function Category({categoryProp}){

	return (
			<Card>
				<Card.Body>
					<Card.Title>
						<h2>{categoryProp.name}</h2>
					</Card.Title>
					<Card.Text>
						{categoryProp.type}
					</Card.Text>
				</Card.Body>
			</Card>
		)
}