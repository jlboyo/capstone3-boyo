import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import 'bootstrap/dist/css/bootstrap.min.css'

let root = document.getElementById('root');

ReactDOM.render(<App />, root);