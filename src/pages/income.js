import React, {useState, useEffect, useContext} from 'react'

import AddIncomeEntry from '../components/AddIncomeEntry'

import {Form, Button, Table, Container, Row, Col} from 'react-bootstrap'

import UserContext from '../userContext'

import { Redirect } from 'react-router-dom'


import '../App.css'


function Income(){

    const {user} = useContext(UserContext)

    
    const [allEntries, setAllEntries] = useState([])
    const [incomeEntries, setIncomeEntries] = useState([])

    const [update, setUpdate] = useState("")

 
    
     useEffect(()=>{

        let token = localStorage.getItem('token')

        fetch('https://nameless-dawn-06532.herokuapp.com/api/entries',{

        method: 'GET',   
        headers: {

                Authorization: `Bearer ${localStorage.getItem('token')}`

                    }
       }).then(res=>res.json())
        .then(data=>{
            console.log(data)
            setAllEntries(data)

            let  entriesTemp = data

            let incomeEntries = entriesTemp.filter(entry=>{

                return entry.type === "income"
            })
             console.log(incomeEntries)
             setIncomeEntries(incomeEntries)
        }).catch(error => {
            "error"
        })


    }, [update])

            const formatter = new Intl.NumberFormat('en-US', {
            stlye: 'currency',
            currency: 'PHP',
            minimumFractionDigits: 2
            })
             let incomeRows = incomeEntries.map(income=>{
                const sign = income.type === "income" ? '+' : '-';
                return (

                    <tr key={income._id}>
                        <td>{income.category}</td>
                        <td className="plus">{sign}₱{formatter.format(income.amount)}</td>
                    </tr>


                    )
             })
            

    return (
        user.email 
					?
       <Container fluid className="con-cat">
          <Row className="justify-content-md-center">
             <Col lg={4} md={4} sm={8}>
           
            <h1>Income</h1>
           <div> 
             <AddIncomeEntry />
           </div> 
           <div>
              <h2>History</h2>
              <Table className="history-table">
                  <thead>
                      <tr>
                          <th>Category</th>
                          <th>Amount</th>
                      </tr>
                  </thead>
                  <tbody>
                      {incomeRows}
                  </tbody>
              </Table>
           </div>
           </Col>
          </Row>
        </Container>
         :
         <Redirect to='/'/>
    )
}

export default Income