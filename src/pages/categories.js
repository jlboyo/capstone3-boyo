import React, {useState,useEffect,useContext} from 'react'

import { Button, Modal, Form, ModalFooter, Container, Col,Row } from 'react-bootstrap';
import {Redirect} from 'react-router-dom'
import Swal from 'sweetalert2'

import UserContext from '../userContext'
import Category from '../components/Category'



export default function Categories(){

	const {user} = useContext(UserContext);

	const [show, setShow] = useState(false);
      
	const handleClose = () => setShow(false);
	const handleShow = () => setShow(true);

	const [name, setName] = useState("")
	const [type, setType] = useState("")

	const [isActive, setIsActive] = useState(true)

	const [allCategories, setAllCategories] = useState([])

	useEffect(()=>{
		fetch('https://nameless-dawn-06532.herokuapp.com/api/categories/',{
			headers: {
				'Authorization': `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => res.json())
		.then(data => {
			setAllCategories(data)
		})
	},[])

	let categoryComponents = allCategories.map			(category=> {
		console.log(category)
	return (
			<Category key={category._id} categoryProp={category}/>
)
})
console.log(categoryComponents)

	useEffect(()=>{
		if(name !== "" && type !==""){
			setIsActive(true)
		} else {
			setIsActive(false)
		}
	},[name, type]);

	function addCategory(e) {
		e.preventDefault()

		let token = localStorage.getItem('token')
		
		fetch('https://nameless-dawn-06532.herokuapp.com/api/categories/',{
			method: 'POST',
			headers: {
				'Content-Type': 'application/json',
				'Authorization': `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({

				name: name,
				type: type
			})
		})
		.then(res => res.json())
		.then(data => {
			if(data.message){
				Swal.fire({
					icon: "success",
					title: "Category Creation Successful.",
					text: `Category has been created.`

				})
			} else {
				console.log(data)
				Swal.fire({	
					icon: "error",
					title: "Category Creation failed.",
					text: data.message
				})
			}
		})
		setName("")
		setType("")
	}


	return (
		user.email 
            ?
		<Container fluid className="con-cat">
			<Row className="justify-content-md-center">
				<Col lg={4} md={4} sm={8}>
				<h1>Categories</h1>
		 <Button onClick={handleShow}>
		 Add Category
		 </Button>	
		 <Modal
				 show={show}
				 onHide={handleClose}
				 backdrop="static"
				 keyboard={false}
		 >
				<Modal.Header closeButton>
					 <Modal.Title>Add Category Form</Modal.Title>
				 </Modal.Header>
				 <Modal.Body>
					 <Form onSubmit={ e => addCategory(e)}>
						 <Form.Group controlId="name">
							 <Form.Label>Name:</Form.Label>
							 <Form.Control 
							 type="text"
							 placeholder="Enter Name"
							 value={name}
							 onChange={(e) => setName(e.target.value)}
							 required
							 />
								 <Form.Text className="text-muted">
								 </Form.Text>
						 </Form.Group>
						 <Form.Group controlId="formGridState">
							<Form.Label>Type:</Form.Label>
						 <Form.Control as="select" defaultValue="Select Type"
						 value={type}
						 onChange={(e) => setType(e.target.value)}
						 required
						 >
								 <option>income</option>
								 <option>expense</option>
						 </Form.Control>
						 </Form.Group>
						 {
					isActive 
					? <Button type="submit" variant="primary">Submit</Button>
					: <Button type="submit" variant="danger" disabled>Submit</Button>
				}
			 </Form>
				 </Modal.Body>
				 <ModalFooter>
					 <Button variant="secondary" onClick={handleClose}>
						 Close
					 </Button>
				 </ModalFooter>
		 </Modal>
		 <h2>Categories:</h2>
		 	<>
			{categoryComponents}
			</>
				</Col>
				</Row>	
 </Container>
 :
 <Redirect to='/'/>
);
}