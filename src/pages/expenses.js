import React, {useState, useEffect, useContext} from 'react'
import { Button, Form, Col, Row, Container, Table } from 'react-bootstrap';


import AddExpenseEntry from '../components/AddExpenseEntry'

import UserContext from '../userContext'

import { Redirect } from 'react-router-dom'

import '../App.css'

function Expenses(){

	const {user} = useContext (UserContext)

 
	const [allEntries, setAllEntries] = useState([])
	const [expenseEntries, setExpenseEntries] = useState([])
 
	const [update, setUpdate] = useState("")

	useEffect(()=>{
		let token = localStorage.getItem('token')

		fetch('https://nameless-dawn-06532.herokuapp.com/api/entries',{

		method: 'GET',   
		headers: {

						Authorization: `Bearer ${localStorage.getItem('token')}`

								}
	 }).then(res=>res.json())
		.then(data=>{
				console.log(data)
				setAllEntries(data)

				let  entriesTemp = data

				let expenseEntries = entriesTemp.filter(entry=>{

						return entry.type === "expense"
				})
				 
				 setExpenseEntries(expenseEntries)
		}).catch(error => {
			"error"
	})



}, [update])    
					 const formatter = new Intl.NumberFormat('en-US', {
					 stlye: 'currency',
					 currency: 'PHP',
					 minimumFractionDigits: 2
					 })
			 
					 let expenseRows = expenseEntries.map(expense=>{
					 const sign = expense.type === "expense" ? '-' : '+';
							
						return (

								<tr key={expense._id}>
										<td>{expense.category}</td>
										<td className="minus">{sign}₱{formatter.format(expense.amount)}</td>
								</tr>


								)
			 
				 })
 
				 return (
					user.email 
					?
					<Container fluid className="con-cat">
					<Row className="justify-content-md-center">
             <Col lg={4} md={4} sm={8}>
						 <h1>Expense</h1>
						 <div>
							<AddExpenseEntry />
						 </div>  
							<div>
								<h2>History</h2>
								<Table className="history-table">
										<thead>
												<tr>
														<th>Category</th>
														<th>Amount</th>
												</tr>
										</thead>
										<tbody>
												{expenseRows}
										</tbody>
								</Table>
						 </div>
             </Col>
          </Row>
					</Container>
					            :
											<Redirect to='/'/>
			)
	}
	
	export default Expenses