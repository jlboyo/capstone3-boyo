import React, {useState,useEffect} from 'react'
import {Form,Button,Container,Col,Row} from 'react-bootstrap'
import Swal from 'sweetalert2'
import {Redirect} from 'react-router-dom'

export default function Register(){
	const [firstName,setFirstName] = useState("")
	const [lastName,setLastName] = useState("")
	const [email,setEmail] = useState("")
	const [mobileNo,setMobileNo] = useState("")
	const [password,setPassword] = useState("")
	const [confirmPassword,setConfirmPassword] = useState("")
	const [isActive,setIsActive] = useState(false)
	const [willRedirect,setWillRedirect] = useState(false)

	useEffect(()=>{
		if((firstName !== "" && lastName !== "" && email !== "" && mobileNo !== "" &&password !== "" && confirmPassword !== "") && (password === confirmPassword) && (mobileNo.length === 11)){
			setIsActive(true)
		} else {
			setIsActive(false)
		}
	},[firstName,lastName,email,mobileNo,password,confirmPassword])

	function registerUser(e){
		e.preventDefault()
		console.log("The page will no longer refresh because of submit.")
		fetch('https://nameless-dawn-06532.herokuapp.com/api/users/',{
			method: "POST",
			headers: {
				"Content-Type": "application/json"
			},
			body: JSON.stringify({
				firstName: firstName,
				lastName: lastName,
				email: email,
				mobileNo: mobileNo,
				password: password,
				confirmPassword: confirmPassword
			})
		})
		.then(response => response.json())
		.then(data => {
			console.log(data)
			if(data.message){
				Swal.fire({
					icon: "error",
					title: "Registration Failed.",
					text: data.message
				})
			} else {
				Swal.fire({
					icon: "success",
					title: "Registration Successful!",
					text: "Thank you for registering."
				})
				setWillRedirect(true)
			}
		})
		setFirstName("")
		setLastName("")
		setMobileNo("")
		setEmail("")
		setPassword("")
		setConfirmPassword("")
	}
	return (
		willRedirect
		?
		<Redirect to="/login" />
		:
		<Form onSubmit={e=>registerUser(e)}>
			<Container fluid >
				<Row className="justify-content-md-center">
					<Col lg={4} md= {4} sm= {12} className="col" className="home-con">
			<Form.Group>
				<Form.Label>First Name:</Form.Label>
				<Form.Control type="text" placeholder="Enter First Name" value={firstName} onChange={e=>{setFirstName(e.target.value)}} required/>
			</Form.Group>
			<Form.Group>
				<Form.Label>Last Name:</Form.Label>
				<Form.Control type="text" placeholder="Enter Last Name" value={lastName} onChange={e=>{setLastName(e.target.value)}} required/>
			</Form.Group>
			<Form.Group>
				<Form.Label>Email:</Form.Label>
				<Form.Control type="email" placeholder="Enter Email" value={email} onChange={e=>{setEmail(e.target.value)}} required/>
			</Form.Group>
			<Form.Group>
				<Form.Label>Mobile Number:</Form.Label>
				<Form.Control type="number" placeholder="Enter 11 Digit Mobile No." value={mobileNo} onChange={e=>{setMobileNo(e.target.value)}} required/>
			</Form.Group>
			<Form.Group>
				<Form.Label>Password:</Form.Label>
				<Form.Control type="password" placeholder="Enter Password" value={password} onChange={e=>{setPassword(e.target.value)}} required/>
			</Form.Group>
			<Form.Group>
				<Form.Label>Confirm Password:</Form.Label>
				<Form.Control type="password" placeholder="Confirm Password" value={confirmPassword} onChange={e=>{setConfirmPassword(e.target.value)}} required/>
			</Form.Group>
			{
				isActive
				? <Button variant="primary" type="submit">Submit</Button>
				: <Button variant="primary" disabled>Submit</Button>
			}
				</Col>
				</Row>
			</Container>
		</Form>
		)
}

