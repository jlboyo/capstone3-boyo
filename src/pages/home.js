import React, {useState, useEffect, useContext} from 'react'

import AddIncomeEntry from '../components/AddIncomeEntry'

import {Form, Button, Table, Container} from 'react-bootstrap'

import UserContext from '../userContext'

import { Redirect } from 'react-router-dom'

import '../App.css'



function Home({totalBalance}) {
        const {user} = useContext(UserContext)
    
        
        const [allEntries, setAllEntries] = useState([])
        const [incomeEntries, setIncomeEntries] = useState([]);
        const [expenseEntries, setExpenseEntries] = useState([]);
        const [incomeAmountArray, setIncomeAmountArray] = useState();
        const [expenseAmountArray, setExpenseAmountArray] = useState([])
        const [update, setUpdate] = useState("")
    
     
         useEffect(()=>{
            let token = localStorage.getItem('token')
            fetch('https://nameless-dawn-06532.herokuapp.com/api/entries',{
    
            method: 'GET',   
            headers: {
    
                    Authorization: `Bearer ${localStorage.getItem('token')}`
    
                        }
           }).then(res=>res.json())
            .then(data=>{
                console.log(data)
                setAllEntries(data)
    
                let entriesTemp = data
    
                let incomeList= entriesTemp.filter(income=>{
                    return income.type === "income"
                })
    
                let expenseList = entriesTemp.filter(expense=>{
                    return expense.type === "expense"
                })
                console.log(incomeList)
                console.log(expenseList)
                setIncomeEntries(incomeList)
                setExpenseEntries(expenseList)
    
              
          
            }).catch(error => {
                "error"
            })     
    
        }, [update])    
    
            const incomeAmount = incomeEntries.map(amount => amount.amount)
            const incomeTotal = incomeAmount.reduce((acc, item)=>(acc += item), 0).toFixed(2);
    
             const expenseAmount = expenseEntries.map(amount => amount.amount)
             const expenseTotal = expenseAmount.reduce((acc, item)=>(acc += item), 0).toFixed(2);
    
             const balance = incomeTotal - expenseTotal
    
             console.log(balance)
             const formatter = new Intl.NumberFormat('en-US', {
                stlye: 'currency',
                currency: 'PHP',
                minimumFractionDigits: 2
             })
    
                
        return (
            user.email 
            ?
            <Container fluid className="home-con">
               <div className="total-balance">
                <h1>Your Balance</h1>
                <h2>₱{formatter.format(balance)}</h2>
               </div>  
                <div className="income-expense-container">
                    <div className="income-container">
                        <h3>Income</h3>
                        <h4 className="plus">+₱{formatter.format(incomeTotal)}</h4>
                    </div>
                    <div className="pseudo"></div>
                    <div className="expense-container">
                        <h3>Expenses</h3>
                        <h4 className="minus">-₱{formatter.format(expenseTotal)}</h4>
                    </div>
                </div>
    
            </Container>
            :
            <Redirect to='/'/>
        )
    }
    
    export default Home