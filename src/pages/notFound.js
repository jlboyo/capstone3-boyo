import React from 'react'
import Banner from '../components/Banner'

export default function NotFound(){
	let bannerContent={
		title: 'Page not Found',
		description: "We're sorry, the page you requested could not be found. Please go back to the homepage",
		label: "Back to Home",
		destination: "/",
	}
	
	return (
			<Banner bannerProps={bannerContent}/>
		)
}