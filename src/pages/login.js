import React, {useState,useEffect,useContext} from 'react'
import {Form,Button,Container,Col,Row,Jumbotron} from 'react-bootstrap'
import Swal from 'sweetalert2'
import UserContext from '../userContext'
import {Redirect} from 'react-router-dom'
import '../App.css'

export default function Login(){
	const {user,setUser} = useContext(UserContext)
	const [email,setEmail] = useState("")
	const [password,setPassword] = useState("")
	const [isActive,setIsActive] = useState(false)
	const [willRedirect,setWillRedirect] = useState(false)

	useEffect(()=>{
		if(email !== "" && password !== ""){
			setIsActive(true)
		} else {
			setIsActive(false)
		}
	},[email,password])

	function loginUser(e){
		e.preventDefault()
		fetch('https://nameless-dawn-06532.herokuapp.com/api/users/login',{
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				email: email,
				password: password
			})
		})
		.then(res => res.json())
		.then(data => {
			if(data.message){
				Swal.fire({
					icon: "error",
					title: "Login failed.",
					text: data.message,
					// showConfirmButton: false,
					// timer: 1500
				})
			} else {
				console.log(data)
				localStorage.setItem('token',data.accessToken)
				fetch('https://nameless-dawn-06532.herokuapp.com/api/users/',{
					headers: {
						Authorization: `Bearer ${data.accessToken}`
					}
				})
				.then(res => res.json())
				.then(data => {
					console.log(data)
					localStorage.setItem('email',data.email)
					localStorage.setItem('isAdmin',data.isAdmin)
					setUser({
						email: data.email,
						isAdmin: data.isAdmin
					})
					setWillRedirect(true)
					
					Swal.fire({
						icon: "success",
						title: "Successful Log In!",
						text: `Thank you for logging in, ${data.firstName}`,
						showConfirmButton: false,
						timer: 1500
					})
				})
			}
		})
		setEmail("")
		setPassword("")
	}

	return (
			user.email || willRedirect
			?
			<Redirect to='/home'/>
			:
			<>
			<Form onSubmit={e => loginUser(e)} >
				<Container fluid>
				<Row className="justify-content-lg-center">
					<Col lg={8} md={6} sm={12} className="col" className="home-con">
					<Jumbotron className="jbtron" fluid>		
						<h1>Viisumus</h1>
	  					<h3>Budget Tracking Application</h3>
					</Jumbotron>
					</Col>
					<Col lg={4} md= {5} sm= {12} className="col" className="home-con">
					<Form.Group controlId="userEmail">
					<Form.Label>
						Email:
					</Form.Label>
					<Form.Control type="email" placeholder="Enter Email" value={email} onChange={(e)=>setEmail(e.target.value)} required />
				</Form.Group>
				<Form.Group controlId="userPassword">
					<Form.Label>
						Password:
					</Form.Label>
					<Form.Control type="password" placeholder="Enter Password" value={password} onChange={(e)=>setPassword(e.target.value)}required />
				</Form.Group>
				{
					isActive
					?
					<Button variant="primary" type="submit">Login</Button>
					:
					<Button variant="primary" disabled>Login</Button>
				}
					</Col>
				</Row>
				</Container>
			</Form>
			</>
	)
}