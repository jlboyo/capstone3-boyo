import React,{useState} from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import './App.css'
import {Container} from 'react-bootstrap'

import {BrowserRouter as Router} from 'react-router-dom'
import {Route,Switch} from 'react-router-dom'

import NavBar from './components/NavBar'
import Register from './pages/register'
import Login from './pages/login'
import Home from './pages/home'
import NotFound from './pages/notFound'
import Categories from './pages/categories';
import Income from './pages/income';
import Expenses from './pages/expenses';

import {UserProvider} from './userContext'


function App(){


	const [user,setUser] = useState({

   email: localStorage.getItem('email'),
   isAdmin: JSON.parse(localStorage.getItem('isAdmin'))


  })

  	console.log(user)

	function unsetUser(){
  	localStorage.clear()
	}

  return(
   	<>
      	<UserProvider value={{user,setUser,unsetUser}}>
         	<Router>
          		<NavBar className="color-nav"/>
               	<Container>
                  	<Switch>
								<Route exact path="/register" component={Register} />
								<Route exact path="/" component={Login} />
								<Route exact path="/home" component={Home} />
								<Route exact path="/categories" component={Categories} />
								<Route exact path="/income" component={Income} />
								<Route exact path="/expenses" component={Expenses} />
								<Route component={NotFound} />
                  	</Switch>
              	 	</Container>
          	</Router>
        </UserProvider>
      </>
    )
}

export default App;